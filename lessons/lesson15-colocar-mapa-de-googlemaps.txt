Colocar mapa de GoogleMaps

Debemos entrar al link https://www.google.com/maps y buscamos la dirección
que deseamos, para este ejemplo, colocaremos la tienda en el Sambil. Una
vez ubicado, le daremos click en Share y le damos a mapa embebido para
copiar el link que usaremos en nuestro código.

Desde ahí podremos escoger el tamaño.