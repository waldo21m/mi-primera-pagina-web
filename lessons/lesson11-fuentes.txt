Fuentes

Se pueden colocar en el font-family fuentes personalizadas. Pero, esta no se
mostrará SINO ESTÁ INSTALADO EN EL COMPUTADOR de la persona quien visita la
página web.

También podemos enviar junto con la página la fuente que quiere que se
despliegue en el sitio web. Para ello, podemos usar el servicio de Google
Fonts.